define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'wxwork/depart/index' + location.search,
                    // add_url: 'wxwork/depart/add',
                    // edit_url: 'wxwork/depart/edit',
                    // del_url: 'wxwork/depart/del',
                    multi_url: 'wxwork/depart/multi',
                    import_url: 'wxwork/depart/import',
                    table: 'wxwork_depart',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'depart_name', title: __('Depart_name'), operate: 'LIKE'},
                        {field: 'depart_en', title: __('Depart_en'), operate: 'LIKE'},
                        {field: 'depart_leader', title: __('Depart_leader'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'sorts', title: __('Sorts')},
                        {field: 'depart_area', title: __('Depart_area'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'state_text', title: __('State')},
                        {field: 'is_show_text', title: __('Is_show')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            // 启动和暂停按钮
            $(document).on("click", ".btn-start,.btn-pause", function () {
              /*  //在table外不可以使用添加.btn-change的方法
                //只能自己调用Table.api.multi实现
                //如果操作全部则ids可以置为空
                var ids = Table.api.selectedids(table);
                Table.api.multi("changestatus", ids.join(","), table, this);*/
                $.ajax({
                    //请求的方式：get/post
                    type:'get',
                    //请求的url地址
                    url:'wxwork/depart/synchronous',
                    //这次请求要携带的数据（不需要参数可以省略）
                    data:{},
                    //请求成功之后的回调函数
                    success:function(data, ret){
                        console.log('data.code', data.code);
                        console.log('data.msg', data.msg);
                       /* if(data.code == 1){
                            Layer.alert(data.msg);
                        }else{
                            Layer.alert(data.msg);
                        }*/
                        Layer.alert(data.msg);
                        table.bootstrapTable('refresh', {});
                    },
                    error:function (data, ret){
                        console.log('data',data);
                        console.log('error_ret',ret);
                    },
                })
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
