define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'wxwork/welcome_demo/index' + location.search,
                    add_url: 'wxwork/welcome_demo/add',
                    edit_url: 'wxwork/welcome_demo/edit',
                    del_url: 'wxwork/welcome_demo/del',
                    multi_url: 'wxwork/welcome_demo/multi',
                    import_url: 'wxwork/welcome_demo/import',
                    table: 'wxwork_welcome_demo',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'wd_cate_text', title: __('Wd_cate')},
                        {field: 'wd_template_id', title: __('Wd_template_id')},
                        {field: 'wd_in_use_text', title: __('Wd_in_use')},
                        {field: 'wd_prior_text', title: __('Wd_prior')},
                        {field: 'admin.username', title: __('Admin.username'), operate: false},
                        {field: 'admin.nickname', title: __('Admin.nickname'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
