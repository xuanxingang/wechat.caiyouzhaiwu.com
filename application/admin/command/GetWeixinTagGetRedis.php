<?php

namespace app\admin\command;



use app\admin\server\QyApiWeixinRedisServer;
use think\Cache;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;

/**
 * 获取每个企业成员的所有标签
 */
class GetWeixinTagGetRedis extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('get_weixin_tag_redis')
            ->addOption('is_go', null, Option::VALUE_OPTIONAL, 'Enable collaboration', 1) // 是否开启协程:0=不开启,1=开启
            ->setDescription('the get_weixin_tag_redis command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
//        $output->writeln('get_weixin_tag_redis');

        //同步最新数据
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $is_go = true;
        if($input->hasOption('is_go')) {
            $is_go = (int)$input->getOption('is_go');
            $is_go = $is_go > 1 ? true : false;
        }
        try {
            self::getUserIdTagRedis($is_go);
        } catch(Exception $e) {
            if($is_go) {
                echo $e->getMessage();
            }
        }
        if($is_go) {
//            echo 'SUCCESS';
            return true;
        }
        return true;
    }

    public static function setTagGetListRedis($is_go = true)
    {
        $data = QyApiWeixinRedisServer::getTagListRedis();
        if(!empty($data)) {
            $tag_total = count($data);
//            $tag_get_len = RedisServer::getLenRedis(QyApiWeixinRedisServer::CGI_BIN_TAG_GET);
            $tag_get_list = Cache::get(QyApiWeixinRedisServer::CGI_BIN_TAG_GET,null);
            !empty($tag_get_list) && $tag_get_list = json_decode($tag_get_list,true);
            if(!empty($tag_get_list)){
                $tag_get_len = count($tag_get_list);
                if(!$tag_get_len || $tag_total != $tag_get_len) {
                    if($is_go) {
                        foreach($data as $item) {
                            QyApiWeixinRedisServer::setTagGetRedis($item['tagid']);
                            sleep(1);
                        }
                    } else {
                        foreach($data as $item) {
                            QyApiWeixinRedisServer::setTagGetRedis($item['tagid']);
                            sleep(1);
                        }
                    }
                }
            }
        }
        return true;
    }

    public static function getUserIdTagRedis($is_go = true, $userid = '')
    {
        try {
            $data = QyApiWeixinRedisServer::getTagListRedis();
            if(!empty($data)){
                $tag_total = count($data);
                $tag_get_list = Cache::get(QyApiWeixinRedisServer::CGI_BIN_TAG_GET,null);
                !empty($tag_get_list) && $tag_get_list = json_decode($tag_get_list,true);
                if(!empty($tag_get_list)){
                    $tag_get_len  = count($tag_get_list);
                    if(!$tag_total || !$tag_get_len || $tag_total != $tag_get_len) {
                        self::setTagGetListRedis($is_go);
                    }
                }
            }else{
                throw new Exception('未获取到标签');
            }
//            $tag_get_len = RedisServer::getLenRedis(QyApiWeixinRedisServer::CGI_BIN_TAG_GET);
//            $data = RedisServer::getHashRedis(QyApiWeixinRedisServer::USER_ID_TAG, $userid);
            $data = Cache::get(QyApiWeixinRedisServer::USER_ID_TAG,null);
            !empty($data) && $data = json_decode($data,true);
            if(empty($data)){
                QyApiWeixinRedisServer::setUserIdTagRedis();
            }else{
                if(!isset($data[$userid])){
                    QyApiWeixinRedisServer::setUserIdTagRedis();
                }
            }
//            $data = RedisServer::getHashRedis(QyApiWeixinRedisServer::USER_ID_TAG, $userid);
            $data = Cache::get(QyApiWeixinRedisServer::USER_ID_TAG,null);
            !empty($data) && $data = json_decode($data,true);
            if(!empty($data)) {
//                QyApiWeixinRedisServer::setUserIdTagRedis();
//                $data = RedisServer::getHashRedis(QyApiWeixinRedisServer::USER_ID_TAG, $userid);
                if(isset($data[$userid])){
                    return  $data[$userid];
                }
            }
            return $data??[];
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
