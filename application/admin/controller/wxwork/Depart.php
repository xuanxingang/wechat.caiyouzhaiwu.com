<?php

namespace app\admin\controller\wxwork;

use app\admin\server\DepartServer;
use app\common\controller\Backend;
use think\Exception;

/**
 *
 *
 * @icon fa fa-circle-o
 */
class Depart extends Backend
{

    /**
     * DepartModel模型对象
     * @var \app\admin\model\wxwork\DepartModel
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\wxwork\DepartModel;
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看
     */
    public function index()
    {
       /* $data =  DepartServer::getTree();
        var_dump('$data',$data);
        exit();*/
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where)
                ->order('sorts', 'desc')
                ->order($sort, $order)
                ->paginate($limit);

            foreach($list as $row) {
                $row->visible(['id', 'depart_name', 'depart_en', 'depart_leader', 'sorts', 'depart_area',
                    'create_time', 'state', 'is_show', 'is_show_text', 'state_text']);
                $row->is_show_text = $row->is_show_text;
                $row->state_text = $row->state_text;
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function synchronous()
    {
        try {
            DepartServer::synchronous();
        } catch(Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success('同步成功');
    }
}
