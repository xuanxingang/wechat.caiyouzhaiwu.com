<?php

namespace app\admin\controller;

use app\admin\server\QiweiNewApplicationsServer;
use app\admin\server\QiweiServer;
use app\common\controller\Backend;
use think\Exception;
use think\Request;

class Qwwechat extends Backend
{
    protected $noNeedLogin = ['notice','verifyurl','verifyurlnewapplications','verifyurltxl'];
    protected $noNeedRight = ['notice','verifyurl','verifyurlnewapplications','verifyurltxl'];

    /**
     * 微信客服:添加配置时的url 验证
     * @param Request $request
     * @return void
     */
    public function verifyurl2(Request $request)
    {
        $echostr = $request->param('echostr', '');
        $msg_signature = $request->param('msg_signature', '');
        $timestamp = $request->param('timestamp', '');
        $nonce = $request->param('nonce', '');
        try {
            $server = new QiweiServer();
            $result = $server->VerifyURL($echostr, $msg_signature, $timestamp, $nonce);
            echo $result;
            // file_put_contents('./result.txt', $result);
        } catch(Exception $e) {
            // file_put_contents('./result_err.txt', $e->getMessage());
        }
    }
    public function verifyurl(Request $request)
    {
        $echostr = $request->param('echostr', '');
        $msg_signature = $request->param('msg_signature', '');
        $timestamp = $request->param('timestamp', '');
        $nonce = $request->param('nonce', '');
        try {
            $server = new QiweiServer();
            $result = $server->VerifyURL($echostr, $msg_signature, $timestamp, $nonce);
            echo $result;
            // file_put_contents('./result.txt', $result);
        } catch(Exception $e) {
            // file_put_contents('./result_err.txt', $e->getMessage());
        }
    }

    public function verifyurltxl(Request $request)
    {
        $echostr = $request->param('echostr', '');
        $msg_signature = $request->param('msg_signature', '');
        $timestamp = $request->param('timestamp', '');
        $nonce = $request->param('nonce', '');

        file_put_contents('./verifyurltxl.txt', 'echostr=='.$echostr.PHP_EOL.PHP_EOL);
        file_put_contents('./verifyurltxl.txt', 'msg_signature=='.$msg_signature.PHP_EOL.PHP_EOL,FILE_APPEND);
        file_put_contents('./verifyurltxl.txt', 'timestamp=='.$timestamp.PHP_EOL.PHP_EOL,FILE_APPEND);
        file_put_contents('./verifyurltxl.txt', 'nonce=='.$nonce.PHP_EOL.PHP_EOL,FILE_APPEND);

        try {
            $server = new QiweiServer();
            $result = $server->DoVerifyURLTxl($echostr, $msg_signature, $timestamp, $nonce);
            echo $result;
            // file_put_contents('./result.txt', $result);
        } catch(Exception $e) {
            // file_put_contents('./result_err.txt', $e->getMessage());
        }
    }

    /**
     * 微信客服
     * @param Request $request
     * @return void
     */
    public function verifyurlnewapplications(Request $request)
    {
        $echostr = $request->param('echostr', '');
        $msg_signature = $request->param('msg_signature', '');
        $timestamp = $request->param('timestamp', '');
        $nonce = $request->param('nonce', '');
        try {
            $server = new QiweiNewApplicationsServer();
            $result = $server->VerifyURL($echostr, $msg_signature, $timestamp, $nonce);
            echo $result;
            // file_put_contents('./result.txt', $result);
        } catch(Exception $e) {
            // file_put_contents('./result_err.txt', $e->getMessage());
        }
    }
    public function  notice(Request $request){

    }
}