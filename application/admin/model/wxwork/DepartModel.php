<?php

namespace app\admin\model\wxwork;

use app\admin\model\BaseModel;


class DepartModel extends BaseModel
{

    // 表名
    protected $name = 'wxwork_depart';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $deleteTime = 'delete_time';

    // 追加属性
    protected $append = [
    ];


    const STATE = [
        self::STATUS_YES => '启用',
        self::STATUS_NO  => '禁用',
    ];
    const IS_SHOW = [
        self::STATUS_YES => '显示',
        self::STATUS_NO  => '隐藏',
    ];

    public function getStateTextAttr($value, $data)
    {
        if(!isset($data['state'])) return '';
        return  self::STATE[$data['state']]??'';
    }

    public function getIsShowTextAttr($value, $data)
    {
        if(!isset($data['is_show'])) return '';
        return  self::IS_SHOW[$data['is_show']]??'';
    }

    public function getCreateTimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['create_time']) ? $data['create_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getDeleteTimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['delete_time']) ? $data['delete_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setCreateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setDeleteTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
