<?php

namespace app\admin\model\wxwork;

use think\Model;

class WelcomeDemoUserListModel  extends Model
{
    // 表名
    protected $name = 'wxwork_welcome_demo_user_list';
}