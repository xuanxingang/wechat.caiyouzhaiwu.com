<?php

namespace app\admin\model\wxwork;

use think\Model;
use traits\model\SoftDelete;


class WelcomeDemoModel extends Model
{
    use SoftDelete;
    //定义数据表中软删除标记字段

    const STATUS_YES = 1;
    const STATUS_NO = 2;

    const STATUS = [
        self::STATUS_YES => '是',
        self::STATUS_NO  => '否',
    ];

    const CATE_NEW_CUSTOMER =  1;
    const CATE_JOIN_GROUP =  2;
    const CATE_WECHAT_SERVICE=  3;
    const CATE = [
        self::CATE_NEW_CUSTOMER => '新客户欢迎语',
        self::CATE_JOIN_GROUP => '入群欢迎语',
        self::CATE_WECHAT_SERVICE => '微信客服会话欢迎语（仅文本）',
    ];


    // 表名
    protected $name = 'wxwork_welcome_demo';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'update_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
    //5.1以上版本才有defaultSoftDelete属性，设置软删除字段的默认值，5.0的默认值为NULL
    protected $defaultSoftDelete = 0;
    // 追加属性
    protected $append = [
        'update_time_text',
        'delete_time_text'
    ];


    public function getUpdateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['update_time']) ? $data['update_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getwdcateTextAttr($value, $data)
    {
        if(!isset($data['wd_cate'])) {
            return '';
        }
        return self::STATUS[$data['wd_cate']] ?? '';
    }

    public function getWdInUseTextAttr($value, $data)
    {
        if(!isset($data['wd_in_use'])) {
            return '';
        }
        return self::STATUS[$data['wd_in_use']] ?? '';
    }

    public function getWdPriorTextAttr($value, $data)
    {
        if(!isset($data['wd_prior'])) {
            return '';
        }
        return self::STATUS[$data['wd_prior']] ?? '';
    }

    public function getDeleteTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['delete_time']) ? $data['delete_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUpdateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setDeleteTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
