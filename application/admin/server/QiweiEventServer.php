<?php

namespace app\admin\server;

use think\Cache;
use think\Exception;

/**
 * 企微事件
 */
class QiweiEventServer extends BaseServer
{
    const  KF_MSG_OR_EVENT_TOKEN = 'kf_msg_or_event_token';//

    public static function index($data)
    {
        $data = self::xmlToArray2($data);
        if(empty($data) || !isset($data['Event']) || !isset($data['MsgType'])) {
            throw new Exception('参数错误');
        }
        if($data['MsgType'] !== 'event') {
            throw new Exception('MsgType参数错误');
        }
        if($data['Event']) {
            switch($data['Event']) {
                case 'change_external_contact':// 企业客户事件
                    switch($data['ChangeType']) {
                        case 'add_external_contact':
                            return self::addExternalContact($data);
                        case 'edit_external_contact':
                            return self::editExternalContact($data);
                        case 'del_external_contact':
                            return self::delExternalContact($data);
                    }
                    break;
                case 'kf_msg_or_event':// 会话分配与消息收发 /  接收消息和事件
                    return self::kfMsgOrEvent($data);
                    break;
            }
        }

    }

    /**
     *   会话分配与消息收发 /  接收消息和事件
     * @param $data
     * @return void
     */
    public static function kfMsgOrEvent($data)
    {
        /**
         * '参数' => '说明',
         * 'ToUserName' => '企业微信CorpID',
         * 'CreateTime' => '消息创建时间，unix时间戳',
         * 'MsgType' => '消息的类型，此时固定为：event',
         * 'Event' => '事件的类型，此时固定为：kf_msg_or_event',
         * 'Token' => '调用拉取消息接口时，需要传此token，用于校验请求的合法性',
         * 'OpenKfId' => '有新消息的客服账号。可通过sync_msg接口指定open_kfid获取此客服账号的消息',
         */
        //回调事件返回的token字段，10分钟内有效；可不填，如果不填接口有严格的频率限制。
        //不多于128字节
//        $data['Token']; //	调用拉取消息接口时，需要传此token，用于校验请求的合法性
        Cache::set(self::KF_MSG_OR_EVENT_TOKEN, $data['Token'], 60 * 10);
    }


    public static function getKfSyncMsg($cursor = '', $voice_format = 0, $limit = 1000, $open_kfid = '')
    {
        try {
            $access_token = WxworkAccessTokenServer::getWxworkAccessToken();
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/kf/sync_msg?access_token=' . $access_token;

        /**
         * 参数              | 必须 | 类型    |    说明
         * access_token     |  是  | string |    调用接口凭证
         * cursor           |  否  | string |    上一次调用时返回的next_cursor，第一次拉取可以不填。若不填，从3天内最早的消息开始返回。
         * 不多于64字节
         * token            | 否  |  string |    回调事件返回的token字段，10分钟内有效；可不填，如果不填接口有严格的频率限制。
         * 不多于128字节
         * limit            | 否  |  uint32 |    期望请求的数据量，默认值和最大值都为1000。
         * 注意：可能会出现返回条数少于limit的情况，需结合返回的has_more字段判断是否继续请求。
         * voice_format     | 否  |  uint32 |    语音消息类型，0-Amr 1-Silk，默认0。可通过该参数控制返回的语音格式，开发者可按需选择自己程序支持的一种格式
         * open_kfid        | 否  |  string |    指定拉取某个客服账号的消息，否则默认返回有权限的客服账号的消息。当客服账号较多，建议按open_kfid来拉取以获取更好的性能。
         */
        $post_data = [
            'cursor'       => "4gw7MepFLfgF2VC5npN",
            'token'        => "ENCApHxnGDNAVNY4AaSJKj4Tb5mwsEMzxhFmHVGcra996NR",
            'limit'        => $limit,
            'voice_format' => $voice_format,
            "open_kfid"    => $open_kfid
        ];
        $result = self::httpCurlPostRequest($url, $post_data);

    }

    /**
     * 添加企业客户事件
     * @param $data
     * @return void
     */
    public static function addExternalContact($data)
    {

    }

    /**
     * 编辑企业客户事件
     * @param $data
     * @return void
     */
    public static function editExternalContact($data)
    {

    }

    /**
     * 删除企业客户事件
     * @param $data
     * @return void
     */
    public static function delExternalContact($data)
    {

    }


}