<?php

return [
    'Depart_name'      => '名称',
    'Depart_en'        => '英文名称',
    'Depart_leader'    => '部门领导',
    'Depart_parent_id' => '上级部门',
    'Sorts'            => '排序',
    'Depart_area'      => '区域',
    'Create_time'      => '添加时间',
    'Delete_time'      => '删除时间',
    'State'            => '状态',//（1启用2禁用）
    'Is_show'          => '是否显示'//1显示2隐藏
];
