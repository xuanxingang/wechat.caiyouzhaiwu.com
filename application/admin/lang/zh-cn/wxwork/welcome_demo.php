<?php

return [
    'Wd_cate'        => '类型',//1新客户欢迎语2入群欢迎语3微信客服会话欢迎语（仅文本）
    'Wd_template_id' => '入群欢迎语',
    'Wd_content'     => '文本内容',
    'Wd_file_arr'    => '序列化选取的附件',
    'Wd_in_use'      => '是否启用',//1是2否
    'Wd_prior'       => '是否优先调用',//1是2否
    'Admin.username' => '管理员账号',
    'Admin.nickname' => '管理员昵称',
];
